'use strict';

angular.module('starter', [
  'ionic',
  'starter.controllers',
  'starter.services',
  'starter.directives',
  'ngCordova',
  'ngSanitize',
  'ion-datetime-picker',
  'com.2fdevs.videogular',
  'com.2fdevs.videogular.plugins.controls',
  'com.2fdevs.videogular.plugins.overlayplay',
  'com.2fdevs.videogular.plugins.poster',
  'ionicCalendarDisplay',
  'chart.js'
])

.run(function($ionicPlatform, $http, $state, Auth, SpeedDial, Storage, $rootScope, $ionicPopup, $cordovaNetwork, $cordovaToast, $ionicHistory, $cordovaStatusbar, $cordovaSplashscreen, $timeout, $ionicPickerI18n){
  $rootScope.showSplash = true;
  $rootScope.Web_EndPoint = 'https://masterpeacecoaching.com/speeddialapp/';
  $rootScope.PHP_EndPoint = 'https://masterpeacecoaching.com/speeddialapp/app/';

  $rootScope.goBack = function(){
    $state.go('app.speeddial');
  };

  $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  $rootScope.videoPlayers = [];
  $rootScope.audioPlayers = [];

  $ionicPickerI18n.cancelClass = "button-default";

  function UpdateTokenToServer(token){
    var params = {
      regId: token,
      userid: Storage.get('UID')
    };
    Auth.SaveRegistrationId(params).then(function(res){
      console.log('token: '+token);
    });
  }

  $rootScope.updateRegistrationId = function(){
    FCMPlugin.onTokenRefresh(function(token){
      UpdateTokenToServer(token);
    });
    FCMPlugin.getToken(function(token){
      UpdateTokenToServer(token);
    });
  }

  $rootScope.FormatDate = function(date){
    var d = new Date(date),
    month = ''+(d.getMonth()+1),
    day = ''+d.getDate(),
    year = d.getFullYear();

    if(month.length < 2) month = '0'+month;
    if(day.length < 2) day = '0'+day;

    return [year, month, day].join('-');
  };

  $rootScope.SetLocalNotify = function(user){
    var notify = {
      userID: user.User_ID
    };
    SpeedDial.GetLocalNotificationByUser(notify).then(function(server){
      if(server.code == 0){
        for(var i in server.notifications){
          var stats = {
            date: server.notifications[i].Noti_Date,
            userid: server.notifications[i].User_ID
          };
          SpeedDial.GetStats(stats).then(function(res){
            if(res.code == 0){
              if(res.CompleteRocks != res.TotalRocks){
                $rootScope.notification(user, server.notifications[i].Noti_Date, server.notifications[i].Local_ID);
              }
            }
          });
        }
      }
    });
  };

  $rootScope.notification = function(user, notifyDate, localId){
    if(!window.cordova){
      console.log('Device not supported cordova.');
      return;
    }

    if(user.User_Reminder == '1'){
      var dd = new Date(user.Reminder_Time);
      var notiTime = {
        hours: dd.getHours(),
        minutes: dd.getMinutes()
      };
      var notiDate = notifyDate.split('-');
      var time = new Date(notiDate[0], (notiDate[1]-1), notiDate[2], notiTime.hours, notiTime.minutes);
      cordova.plugins.notification.local.schedule({
        id: localId,
        trigger: {
          at: time
        },
        text: "How many Inspired Actions have you accomplished today?",
        title: "SpeedDial",
        data: {
          path: 'app.rocks',
          date: notifyDate,
          userid: user.User_ID
        }
      });
    }else{
      cordova.plugins.notification.local.cancelAll(function(){
        console.log('all local notifications have been cleared');
      });
    }
  }

  $ionicPlatform.ready(function(){

    if(ionic.Platform.isIOS() && window.cordova){
      cordova.plugins.notification.local.hasPermission(function(granted){
        if(!granted){
          cordova.plugins.notification.local.promptForPermission();
        }
      });
    }

    if(navigator.splashscreen) {
      $cordovaSplashscreen.hide();
    }

    if(window.StatusBar){
      StatusBar.styleDefault();
      $cordovaStatusbar.overlaysWebView(false);
      if($rootScope.showSplash){
        $cordovaStatusbar.styleHex('#e05729');
      }
    }

    var userData = Auth.isLogged();
    $timeout(function(){
      $rootScope.showSplash = false;
      if(window.StatusBar){
        $cordovaStatusbar.styleHex('#5e3c58');
      }
    }, 2000);

    if(window.cordova && window.cordova.plugins.Keyboard){
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if(userData){
      var device = {
        device: ionic.Platform.platform(),
        userid: Storage.get('UID')
      };
      Auth.updateDevice(device).then(function(res){});
    }

    var myPopup;
    var CheckInternetConnection = function(){
      if(window.Connection){
        if(navigator.connection.type == Connection.NONE){
          myPopup = $ionicPopup.alert({
            title: 'Warning!',
            template: 'Sorry, no internet connection detected. Please reconnect and try again.',
            buttons: [{
              text: '<b>EXIT</b>',
              type: 'button-positive',
              onTap: function(e){
                ionic.Platform.exitApp();
                e.preventDefault();
              }
            }]
          });
        }
      }
    }
    CheckInternetConnection();

    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      $cordovaToast.showLongBottom('Connected successfully.');
      if(myPopup){
        myPopup.close();
      }
    });
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      CheckInternetConnection();
    });

    $ionicPlatform.on('resume', function(){
      if(Storage.get('LogID') == 'undefined' || Storage.get('LogID') == null){
        Auth.createLogId().then(function(res){
          Storage.set('LogID', res.LogID);
        });
      }
    });

    $ionicPlatform.on('pause', function(){
      Auth.logout().then(function(res){
        if(res.code == 0){
          Storage.destroy('LogID');
        }
      });
    });

    screen.orientation.lock('portrait');

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      for(var i = 0; i < $rootScope.videoPlayers.length; i++){
        $rootScope.videoPlayers[i].stop();
      }
      for(var i = 0; i < $rootScope.audioPlayers.length; i++){
        $rootScope.audioPlayers[i].stop();
      }
    });

    var BackButton = 0;
    var ExitMsgShowURIs = ['home', 'app.speeddial'];
    $ionicPlatform.registerBackButtonAction(function(){
      if(ExitMsgShowURIs.indexOf($ionicHistory.currentStateName()) != -1){
        if(BackButton == 0){
          BackButton++;
          $cordovaToast.showLongBottom('Press again to exit');
          $timeout(function(){
            BackButton = 0;
          }, 2000);
        }else{
          if(Storage.get('LogID') != 'undefined' && Storage.get('LogID') != null){
            Auth.logout().then(function(res){
              if(res.code == 0){
                Storage.destroy('LogID');
                ionic.Platform.exitApp();
              }
            });
          }else{
            Storage.destroy('LogID');
            ionic.Platform.exitApp();
          }
        }
      }else{
        var backView = $ionicHistory.backView();
        if(backView == null){
          $rootScope.goBack();
        }else{
          $ionicHistory.goBack();
        }
      }
    }, 100);

    if(window.cordova){
      window.cordova.plugins.notification.local.on('add', function(notification){
        var date = new Date();
        var selectedDate = $rootScope.FormatDate(date);
        var localId = Storage.get('UID')+selectedDate.replace('-', '').replace('-', '');
        var notify = {
          userID: Storage.get('UID'),
          localID: localId,
          notiDate: selectedDate
        };
        SpeedDial.SaveLocalNotify(notify).then(function(response){
          console.log(response.text);
        });
      });

      window.cordova.plugins.notification.local.on('click', function(notification, state){
        var jsonData = notification.data;
        Storage.set('notiDate', jsonData.date);
        Storage.set('notiUserId', jsonData.userid);
        $state.go(jsonData.path);
      });

      FCMPlugin.onNotification(function(data){
        if(data.wasTapped){
          $state.go('app.resources');
          // window.open(data.link, '_blank', 'location=no');
        }else{
          $ionicPopup.alert({
            title: data.title,
            template: data.body,
            buttons: [{
              text: 'OK',
              type: 'button-positive',
              onTap: function(e){
                $state.go('app.resources');
                // window.open(data.link, '_blank', 'location=no');
              }
            }]
          });
        }
      });
    }

  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, ChartJsProvider){
  $ionicConfigProvider.tabs.position('bottom');
  ChartJsProvider.setOptions({
    colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
  });

  $stateProvider

  .state('home', {
    url: '/',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl',
    onEnter: function($state, Auth){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('signup', {
    url: "/signup",
    templateUrl: "templates/signup.html",
    controller: 'SignCtrl',
    onEnter: function($state, Auth, Storage){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('register', {
    url: "/register",
    templateUrl: "templates/register.html",
    controller: 'RegisterCtrl',
    onEnter: function($state, Auth){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('signin', {
    url: "/signin",
    templateUrl: "templates/signin.html",
    controller: 'SignCtrl',
    onEnter: function($state, Auth, Storage){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl',
    onEnter: function($state, Auth){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('forgot', {
    url: "/forgot",
    templateUrl: "templates/forgot.html",
    controller: 'LoginCtrl',
    onEnter: function($state, Auth){
      var userData = Auth.isLogged();
      if(userData){
        $state.go('app.speeddial');
      }
    }
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl',
    onEnter: function($state, Auth, Storage){
      if(Storage.get('LogID') == 'undefined' || Storage.get('LogID') == null){
        Auth.createLogId().then(function(res){
          Storage.set('LogID', res.LogID);
        });
      }

      var userData = Auth.isLogged();
      if(!userData){
        $state.go('signin');
      }
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent' : {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.howtouse', {
    cache: false,
    url: "/howtouse",
    views: {
      'menuContent' : {
        templateUrl: "templates/howtouse.html",
        controller: 'HowtouseCtrl'
      }
    }
  })

  .state('app.rocks', {
    cache: false,
    url: "/rocks",
    views: {
      'menuContent' : {
        templateUrl: "templates/rocks.html",
        controller: 'RocksCtrl'
      }
    }
  })

  .state('app.speeddial', {
    cache: false,
    url: "/speeddial",
    views: {
      'menuContent' : {
        templateUrl: "templates/speeddial.html",
        controller: 'SpeedDialCtrl'
      }
    }
  })

  .state('app.questions', {
    cache: false,
    url: "/questions",
    views: {
      'menuContent' : {
        templateUrl: "templates/questions.html",
        controller: 'QuestionsCtrl'
      }
    }
  })

  .state('app.media', {
    url: "/media",
    views: {
      'menuContent' : {
        templateUrl: "templates/media.html",
        controller: 'MediaCtrl'
      }
    }
  })

  .state('app.media.video', {
    cache: false,
    url: "/video",
    views: {
      'video-tab': {
        templateUrl: "templates/video.html",
        controller: 'MediaCtrl'
      }
    }
  })
  .state('app.media.audio', {
    cache: false,
    url: "/audio",
    views: {
      'audio-tab': {
        templateUrl: "templates/audio.html",
        controller: 'MediaCtrl'
      }
    }
  })

  .state('app.statistics', {
    cache: false,
    url: "/statistics",
    views: {
      'menuContent' : {
        templateUrl: "templates/statistics.html",
        controller: 'StatisticsCtrl'
      }
    }
  })

  .state('app.resources', {
    cache: false,
    url: "/resources",
    views: {
      'menuContent' : {
        templateUrl: "templates/resources.html",
        controller: 'ResourcesCtrl'
      }
    }
  })

  .state('app.consultation', {
    cache: false,
    url: "/consultation",
    views: {
      'menuContent' : {
        templateUrl: "templates/consultation.html",
        controller: 'ConsultationCtrl'
      }
    }
  })

  .state('app.support', {
    url: "/support",
    views: {
      'menuContent' : {
        templateUrl: "templates/support.html",
        controller: 'SupportCtrl'
      }
    }
  })

  .state('app.about', {
    cache: false,
    url: "/about",
    views: {
      'menuContent' : {
        templateUrl: "templates/about.html",
        controller: 'AboutCtrl'
      }
    }
  });

  $urlRouterProvider.otherwise('/');
});
