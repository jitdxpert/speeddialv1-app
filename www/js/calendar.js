angular.module('ionicCalendarDisplay', [])
.filter('rangecal', function(){
  return function(input, total){
    total = parseInt(total);
    for(var i=0; i<total; i++){
      input.push(i);
    }
    return input;
  };
})

.directive('myCalendar', function(Storage, SpeedDial){
  return {
    restrict: 'E',
    transclude: true,
    require: 'display',
    scope: { display:"=", dateformat:"="},
    controller: ['$scope', '$filter', function($scope, $filter){
      function pad(n){
        return (n < 10) ? ("0" + n) : n;
      }
      var SpeedDials = [];
      SpeedDial.getAll().then(function(res){
        if(res.code == 0){
          for(var i in res.text.journal){
            SpeedDials.push(res.text.journal[i].Journal_Date);
          }
        }

        var calMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var calDaysForMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        var selectedYear, selectedMonth, selectedDate, shortMonth;
        var CurrentDate = new Date();

        $scope.calMonths = [[{'id':0,'name':'Jan'},{'id':1,'name':'Feb'},{'id':2,'name':'Mar'},{'id':3,'name':'Apr'}],[{'id':4,'name':'May'},{'id':5,'name':'Jun'},{'id':6,'name':'Jul'},{'id':7,'name':'Aug'}],[{'id':8,'name':'Sep'},{'id':9,'name':'Oct'},{'id':10,'name':'Nov'},{'id':11,'name':'Dec'}]];

        selectedYear = CurrentDate.getFullYear(),
        selectedMonth = CurrentDate.getMonth(),
        selectedDate = CurrentDate.getDate();

        $scope.UICalendarDisplay = {};
        $scope.UICalendarDisplay.Date = true;
        $scope.UICalendarDisplay.Month = false;
        $scope.UICalendarDisplay.Year = false;

        $scope.displayCompleteDate = function(){
          var timeStamp = new Date(selectedYear, selectedMonth, selectedDate).getTime();
          if(angular.isUndefined($scope.dateformat)){
            var format = "dd - MMM - yy";
          }else{
            var format = $scope.dateformat;
          }
          $scope.display = $filter('date')(timeStamp, format);
        }

        //Onload Display Current Date
        $scope.displayCompleteDate();

        $scope.UIdisplayDatetoMonth = function(){
          $scope.UICalendarDisplay.Date = false;
          $scope.UICalendarDisplay.Month = true;
          $scope.UICalendarDisplay.Year = false;
        }

        $scope.UIdisplayMonthtoYear = function(){
          $scope.UICalendarDisplay.Date = false;
          $scope.UICalendarDisplay.Month = false;
          $scope.UICalendarDisplay.Year = true;
        }

        $scope.UIdisplayYeartoMonth = function(){
          $scope.UICalendarDisplay.Date = false;
          $scope.UICalendarDisplay.Month = true;
          $scope.UICalendarDisplay.Year = false;
        }
        $scope.UIdisplayMonthtoDate = function(){
          $scope.UICalendarDisplay.Date = true;
          $scope.UICalendarDisplay.Month = false;
          $scope.UICalendarDisplay.Year = false;
        }

        $scope.selectedMonthPrevClick = function(){
          selectedDate = 1;
          if(selectedMonth == 0){
            selectedMonth = 11;
            selectedYear--;
          }else{
            $scope.dislayMonth = selectedMonth--;
          }
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedMonthNextClick = function(){
          selectedDate = 1;
          if(selectedMonth == 11){
            selectedMonth = 0;
            selectedYear++;
          }else{
            $scope.dislayMonth = selectedMonth++;
          }
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedMonthYearPrevClick = function(){
          selectedYear--;
          $scope.displayYear = selectedYear;
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedMonthYearNextClick = function(){
          selectedYear++;
          $scope.displayYear = selectedYear;
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedDecadePrevClick = function(){
          selectedYear -= 10;
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedDecadeNextClick = function(){
          selectedYear += 10;
          $scope.displayMonthCalendar();
          $scope.displayCompleteDate();
        }

        $scope.selectedYearClick = function(year){
          $scope.displayYear = year;
          selectedYear = year;
          $scope.displayMonthCalendar();
          $scope.UICalendarDisplay.Date = false;
          $scope.UICalendarDisplay.Month = true;
          $scope.UICalendarDisplay.Year = false;
          $scope.displayCompleteDate();
        }

        $scope.selectedMonthClick = function(month){
          $scope.dislayMonth = month;
          selectedMonth = month;
          $scope.displayMonthCalendar();
          $scope.UICalendarDisplay.Date = true;
          $scope.UICalendarDisplay.Month = false;
          $scope.UICalendarDisplay.Year = false;
          $scope.displayCompleteDate();
        }

        $scope.selectedDateClick = function(date){
          $scope.displayDate = date.date;
          selectedDate = date.date;

          if(date.type == 'newMonth'){
            var mnthDate = new Date(selectedYear, selectedMonth, 32)
            selectedMonth = mnthDate.getMonth();
            selectedYear = mnthDate.getFullYear();
            $scope.displayMonthCalendar();
          }else if(date.type == 'oldMonth'){
            var mnthDate = new Date(selectedYear, selectedMonth, 0);
            selectedMonth = mnthDate.getMonth();
            selectedYear = mnthDate.getFullYear();
            $scope.displayMonthCalendar();
          }

          $scope.displayCompleteDate();
        }

        $scope.displayMonthCalendar = function(){
          $scope.startYearDisp = (Math.floor(selectedYear / 10) * 10) - 1;
          $scope.endYearDisp = (Math.floor(selectedYear / 10) * 10) + 10;

          $scope.datesDisp = [[],[],[],[],[],[]];
            countDatingStart = 1;

            if(calMonths[selectedMonth] === 'February'){
              if(selectedYear%4 === 0){
                endingDateLimit = 29;
              }else{
                endingDateLimit = 28;
              }
            }else{
              endingDateLimit = calDaysForMonth[selectedMonth];
            }
            startDay = new Date(selectedYear, selectedMonth, 1).getDay();

          $scope.displayYear = selectedYear;
          $scope.dislayMonth = calMonths[selectedMonth];
          $scope.shortMonth = calMonths[selectedMonth].slice(0,3);

          $scope.displayDate = selectedDate;

          var nextMonthStartDates = 1;
          var prevMonthLastDates = new Date(selectedYear, selectedMonth, 0).getDate();

          for(i = 0; i < 6; i++){
            if(typeof $scope.datesDisp[0][6] === 'undefined'){
              for(j = 0; j < 7; j++){
                if(j < startDay){
                  var dateDay = (prevMonthLastDates - startDay + 1) + j;
                  if(SpeedDials.indexOf((selectedMonth == 0 ? (selectedYear - 1) : selectedYear) + '-' + (selectedMonth == 0 ? 12 : pad(selectedMonth)) + '-' +
                     pad(dateDay)) !== -1){
                    $scope.datesDisp[i][j] = {
                      "type":"oldMonth",
                      "date":dateDay,
                      "journal":true
                    };
                  }else{
                    $scope.datesDisp[i][j] = {
                      "type":"oldMonth",
                      "date":dateDay,
                      "journal":false
                    };
                  }
                }else{
                  var dateDay = countDatingStart++;
                  if(SpeedDials.indexOf(selectedYear + '-' + pad(selectedMonth + 1) + '-' + pad(dateDay)) !== -1){
                    $scope.datesDisp[i][j] = {
                      "type":"currentMonth",
                      "date":dateDay,
                      "journal":true
                    };
                  }else{
                    $scope.datesDisp[i][j] = {
                      "type":"currentMonth",
                      "date":dateDay,
                      "journal":false
                    };
                  }
                }
              }
            }else{
              for(k = 0; k < 7; k++){
                if(countDatingStart <= endingDateLimit){
                  var dateDay = countDatingStart++;
                  if(SpeedDials.indexOf(selectedYear + '-' + pad(selectedMonth + 1) + '-' + pad(dateDay)) !== -1){
                    $scope.datesDisp[i][k] = {
                      "type":"currentMonth",
                      "date":dateDay,
                      "journal":true
                    };
                  }else{
                    $scope.datesDisp[i][k] = {
                      "type":"currentMonth",
                      "date":dateDay,
                      "journal":false
                    };
                  }
                }else{
                  var dateDay = nextMonthStartDates++;
                  if(SpeedDials.indexOf((selectedMonth == 11 ? (selectedYear + 1) : selectedYear) + '-' + (selectedMonth == 11 ? pad(1) : pad(selectedMonth + 2)) + '-' + pad(dateDay)) !== -1){
                    $scope.datesDisp[i][k] = {
                      "type":"newMonth",
                      "date":dateDay,
                      "journal":true
                    };
                  }else{
                    $scope.datesDisp[i][k] = {
                      "type":"newMonth",
                      "date":dateDay,
                      "journal":false
                    };
                  }
                }
               }
             }
          }
        }
        $scope.displayMonthCalendar();
        $scope.displayCompleteDate();
      });
    }],
    template: '<div class="ionic_Calendar">'
    + '  <div class="calendar_Date" ng-show="UICalendarDisplay.Date">'
    + '    <div class="row" style="background-color:#5c596d;color:white">'
    + '      <div class="col txtCenter" ><i class="icon ion-chevron-left" ng-click="selectedMonthPrevClick()"></i></div>'
    + '      <div class="col col-75 txtCenter" ng-click="UIdisplayDatetoMonth()">{{dislayMonth}} {{displayYear}}</div>'
    + '      <div class="col txtCenter"><i class="icon ion-chevron-right"  ng-click="selectedMonthNextClick()"></i></div>'
    + '    </div>'
    + '    <div class="row Daysheading Daysheading_Label" style="background-color:#5c596d;color:white">'
    + '      <div class="col">Sun</div><div class="col">Mon</div><div class="col">Tue</div><div class="col">Wed</div><div class="col">Thu</div><div class="col">Fri</div><div class="col">Sat</div>'
    + '    </div>'
    + '    <div class="row Daysheading DaysDisplay" ng-repeat="rowVal in datesDisp  track by $index" ng-class="{\'marginTop0\':$first}">'
    + '      <div class="col" ng-repeat="colVal in rowVal  track by $index" ng-class="{\'fadeDateDisp\':(colVal.type == \'oldMonth\' || colVal.type == \'newMonth\'), \'selDate\':(colVal.date == displayDate && colVal.type == \'currentMonth\'), \'hasJournal\':colVal.journal == true}" ng-click="selectedDateClick(colVal)">{{colVal.date}}</div> '
    + '    </div>'
    + '  </div>'
    + '  <div class="calendar_Month" ng-show="UICalendarDisplay.Month">'
    + '    <div class="row" style="background-color:#5c596d;color:white">'
    + '      <div class="col txtCenter"><i class="icon ion-chevron-left" ng-click="selectedMonthYearPrevClick()"></i></div>'
    + '      <div class="col col-75 txtCenter" ng-click="UIdisplayMonthtoYear()">{{displayYear}}</div>'
    + '      <div class="col txtCenter"><i class="icon ion-chevron-right" ng-click="selectedMonthYearNextClick()"></i></div>'
    + '    </div>'
    + '    <div class="row txtCenter MonthsDisplay" ng-repeat="rowVal in calMonths  track by $index" ng-class="{\'marginTop0\':$first}">'
    + '      <div class="col" ng-repeat="colVal in rowVal  track by $index"  ng-class="(colVal.name == shortMonth) ? \'selMonth\' : \'NonSelMonth\'"  ng-click="selectedMonthClick(colVal.id)" >{{colVal.name}}</div>'
    + '    </div>'
    + '  </div>'
    + '  <div class="calendar_Year" ng-show="UICalendarDisplay.Year">'
    + '    <div class="row" style="background-color:#5c596d;color:white">'
    + '      <div class="col txtCenter"><i class="icon ion-chevron-left" ng-click="selectedDecadePrevClick()"></i></div>'
    + '      <div class="col col-75 txtCenter">{{startYearDisp+1}}-{{endYearDisp-1}}</div>'
    + '      <div class="col txtCenter"><i class="icon ion-chevron-right" ng-click="selectedDecadeNextClick()"></i></div>'
    + '    </div>'
    + '    <div class="row txtCenter YearsDisplay" ng-repeat="nx in []| rangecal:3" ng-class="{\'marginTop0\':$first}">'
    + '      <div class="col" ng-repeat="n in [] | rangecal:4"  ng-class="{ \'fadeYear\': (((startYearDisp+nx+nx+nx+nx+n) == startYearDisp)||((startYearDisp+nx+nx+nx+nx+n) == endYearDisp)), \'selYear\': ((startYearDisp+nx+nx+nx+nx+n) == displayYear) }" ng-click="selectedYearClick((startYearDisp+nx+nx+nx+nx+n))">{{startYearDisp+nx+nx+nx+nx+n}}</div>'
    + '    </div>'
    + '  </div>'
    + '</div>'
  };
});
