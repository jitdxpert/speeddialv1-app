'use strict';

angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $state){
  $scope.signup = function(){
    $state.go('signup');
  };

  $scope.signin = function(){
    $state.go('signin');
  };
})

.controller('SignCtrl', function($scope, $rootScope, $state, Auth, Storage, SpeedDial, $ionicPopup){
  $scope.FacebookSignIn = function(){
    facebookConnectPlugin.login(['email','public_profile'], function(response){
      var authResponse = response.authResponse;
      facebookConnectPlugin.api('/me?fields=email,name&access_token='+authResponse.accessToken, null, function(profileInfo){
        var dataString = {
          media: 'facebook',
          sId: profileInfo.id,
          name: profileInfo.name,
          email: profileInfo.email,
          sex: null,
          image: "http://graph.facebook.com/"+authResponse.userID+"/picture?type=large",
          screen: null
        };
        Auth.SocialLogin(dataString, $scope).then(function(res){
          if(res.code == 0){
            Storage.set('FB', true);
            Storage.set('UID', res.text.User_ID);
            Storage.set('LogID', res.LogID);
            Storage.set('UserData', res.text);

            $rootScope.updateRegistrationId();
            $rootScope.SetLocalNotify(res.text);

            if(res.first == 1){
              $state.go('app.howtouse');
            }else{
              $state.go('app.speeddial');
            }
          }else{
            $ionicPopup.alert({
              title: 'ERROR',
              template: res.text
            });
          }
        });
      });
    });
  };

  $scope.GoogleSignIn = function(){
    window.plugins.googleplus.login({
      'scopes': '',
      'webClientId': '149145940760-3fboc1jo8579p3dejirvpvuk6q866830.apps.googleusercontent.com',
      'offline': true,
    }, function (obj){
      var dataString = {
        media: 'google',
        sId: obj.userId,
        name: obj.displayName,
        email: obj.email,
        sex: null,
        image: obj.imageUrl,
        screen: null
      };
      Auth.SocialLogin(dataString, $scope).then(function(res){
        if(res.code == 0){
          Storage.set('GL', true);
          Storage.set('UID', res.text.User_ID);
          Storage.set('LogID', res.LogID);
          Storage.set('UserData', res.text);

          $rootScope.updateRegistrationId();
          $rootScope.SetLocalNotify(res.text);

          if(res.first == 1){
            $state.go('app.howtouse');
          }else{
            $state.go('app.speeddial');
          }
        }else{
          $ionicPopup.alert({
            title: 'ERROR',
            template: res.text
          });
        }
      });
    });
  };

  $scope.register = function(){
    $state.go('register');
  };

  $scope.login = function(){
    $state.go('login');
  };
})

.controller('RegisterCtrl', function($scope, $state, Auth, Storage, $ionicPopup){
  $scope.RegisterProcess = function(register){
    Auth.register(register, $scope).then(function(res){
      if(res.code == 0){
        Storage.set('UID', res.text.User_ID);
        Storage.set('LogID', res.LogID);
        Storage.set('UserData', res.text);

        $rootScope.updateRegistrationId();
        $rootScope.SetLocalNotify(res.text);

        $state.go('app.howtouse');
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    });
  };
})

.controller('LoginCtrl', function($scope, $rootScope, $state, Auth, Storage, SpeedDial, $ionicPopup){
  $scope.SigninProcess = function(signin){
    Auth.login(signin, $scope).then(function(res){
      if(res.code == 0){
        Storage.set('UID', res.text.User_ID);
        Storage.set('LogID', res.LogID);
        Storage.set('UserData', res.text);

        $rootScope.updateRegistrationId();
        $rootScope.SetLocalNotify(res.text);

        $state.go('app.speeddial');
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    });
  };

  $scope.ForgotProcess = function(forgot){
    Auth.forgot(forgot, $scope).then(function(res){
      if(res.code == 0){
        $state.go('login');
        $ionicPopup.alert({
          title: 'SUCCESS',
          template: res.text
        });
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    });
  };
})

.controller('AppCtrl', function($scope, $rootScope, $state, Auth, Storage, $ionicSideMenuDelegate, $ionicPopup, $ionicPopover, $cordovaStatusbar){
  $scope.$ionicSideMenuDelegate = $ionicSideMenuDelegate;

  $scope.$on('$ionicView.enter', function(){
    Auth.CurrentUser().then(function(res){
      if(res.code == 0){
        $rootScope.userData = res.text;
      }else if(res.code == 2){
        $state.go('app.howtouse');
      }else{
        $state.go('home');
      }
    });
  });

  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover){
    $scope.popover = popover;
  });

  $scope.logout = function(){
    $ionicPopup.confirm({
      title: 'CONFIRM LOGOUT?',
      template: 'Are you sure you want to logout?'
    }).then(function(res){
      if(res){
        var logout = {
          logId: Storage.get('LogID'),
          userId: Storage.get('UID')
        }
        Auth.logout(logout).then(function(res){
          if(res.code == 0){
            if(window.cordova){
              cordova.plugins.notification.local.clearAll();
            }
            FCMPlugin.unsubscribeFromTopic('all');
            if(Storage.get('FB')){
              Storage.destroy('FB');
              facebookConnectPlugin.logout(function(){
                console.log('SUCCESS Logged out from facebook');
              }, function(fail){
                console.log('FAILED Logged out from facebook');
              });
            }
            if(Storage.get('GL')){
              Storage.destroy('GL');
              window.plugins.googleplus.logout(function(msg){
                console.log(msg);
              });
            }
            $rootScope.userData = [];
            Storage.destroy('UID');
            Storage.destroy('LogID');
            if(Storage.get('selectedDate')){
              Storage.destroy('selectedDate');
            }
            if(Storage.get('notiDate')){
              Storage.destroy('notiDate');
            }
            if(Storage.get('notiUserId')){
              Storage.destroy('notiUserId');
            }
            $state.go('home');
          }
        });
      }
    });
  };
})

.controller('HowtouseCtrl', function($scope, $sce, HowToUse){
  $scope.$on('$ionicView.enter', function(){
    $scope.SpeedDialContent = 'Loading speed dial content...';

    HowToUse.GetSpeedDialContent().then(function(res){
      if(res.code == 0){
        $scope.SpeedDialContent = $sce.trustAsHtml(res.text.Page_Content);
      }
    });
  });
})

.controller('ProfileCtrl', function($scope, Auth, Storage, $rootScope, $cordovaToast, $ionicModal, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $ionicActionSheet){
  $scope.$on('$ionicView.enter', function(){
    Auth.CurrentUser().then(function(res){
      if(res.code == 0){
        $scope.profile = {
          name: res.text.User_FullName,
          gender: res.text.User_Gender,
          desc: res.text.User_Desc,
          mobile: res.text.User_Mobile,
          skype: res.text.User_Skype,
          userid: res.text.User_ID,
          reminder: res.text.User_Reminder == '1' ? true : false,
          time: res.text.Reminder_Time ? res.text.Reminder_Time : new Date(),
        };
      }
    });
  });

  $scope.loadImage = function(){
    var hideSheet = $ionicActionSheet.show({
      titleText: 'Choose Image From',
      buttons: [
        { text: '<i class="icon ion-images"></i> Open Gallery' },
        { text: '<i class="icon ion-camera"></i> Take Photo' },
      ],
      cancelText: 'Cancel',
      cancel: function(){
        return true;
      },
      buttonClicked: function(index){
        var type = null;
        if(index === 0){
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        }else if(index === 1){
          type = Camera.PictureSourceType.CAMERA;
        }
        if(type !== null){
          $scope.selectPicture(type);
          return true;
        }
      }
    });
  };

  $scope.selectPicture = function(sourceType){
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      allowEdit: true,
      saveToPhotoAlbum: false,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation:true
    };
    $cordovaCamera.getPicture(options).then(function(imagePath){
      var currentName = imagePath.replace(/^.*[\\\/]/, '');

      var d = new Date(),
      n = d.getTime(),
      newFileName = n+".jpg";

      if($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY){
        window.FilePath.resolveNativePath(imagePath, function(entry){
          window.resolveLocalFileSystemURL(entry, success, fail);
          function fail(e){
            $ionicPopup.alert({
              title: 'ERROR',
              template: e
            });
          }
          function success(fileEntry){
            var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/')+1);
            $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.uploadImage(newFileName);
            }, function(error){
              $ionicPopup.alert({
                title: 'ERROR',
                template: error.exception
              });
            });
          }
        });
      }else{
        var namePath = imagePath.substr(0, imagePath.lastIndexOf('/')+1);
        $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
          $scope.uploadImage(newFileName);
        }, function(error){
          $ionicPopup.alert({
            title: 'ERROR',
            template: error.exception
          });
        });
      }
    },
    function(err){
      console.log('Not always an error, maybe cancel was pressed...');
    });
  };

  $scope.pathForImage = function(image){
    if(image === null){
      return '';
    }else{
      return cordova.file.dataDirectory+image;
    }
  };

  $scope.uploadImage = function(image){
    var targetPath = $scope.pathForImage(image);
    var filename = image;
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
    $cordovaFileTransfer.upload($rootScope.PHP_EndPoint+'?case=UploadProfileImage&UserID='+Storage.get('UID'), targetPath, options).then(function(result){
      if(result.responseCode == 200){
        Auth.CurrentUser().then(function(res){
          if(res.code == 0){
            $rootScope.userData = res.text;
          }
        });
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: result.text
        });
      }
    });
  };

  $ionicModal.fromTemplateUrl('templates/password.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal){
    $scope.modal = modal;
  });
  $scope.openModal = function(){
    $scope.modal.show();
  };
  $scope.closeModal = function(){
    $scope.modal.hide();
  };

  $scope.password = {
    current: '',
    new: '',
    confirm: '',
    userid: Storage.get('UID')
  };
  $scope.PasswordProcess = function(password){
    Auth.changePassword(password).then(function(res){
      $cordovaToast.showLongBottom(res.text);
      $scope.closeModal();
      $scope.password = {
        current: '',
        new: '',
        confirm: '',
        userid: Storage.get('UID')
      };
    });
  };

  $scope.SetReminder = function(profile){
    Auth.setReminder(profile).then(function(res){
      $cordovaToast.showLongBottom(res.text);
      Auth.CurrentUser().then(function(res){
        $rootScope.SetLocalNotify(res.text);
      });
    });
  };

  $scope.$watch("profile.time", function(newValue, oldValue){
    if(typeof oldValue === 'undefined' || typeof newValue === 'undefined') return;
    if(oldValue === newValue) return;

    var profile = {
      time: newValue,
      userid: $scope.profile.userid
    };
    Auth.setReminderTime(profile).then(function(result){
      Auth.CurrentUser().then(function(res){
        $rootScope.SetLocalNotify(res.text);
        $cordovaToast.showLongBottom(result.text);
      });
    });
  });
})

.controller('RocksCtrl', function($scope, $state, Storage, SpeedDial, $cordovaToast){
  $scope.$on('$ionicView.enter', function(){
    $scope.rocks = {};
    $scope.items = [];
    $scope.loading = true;
    var getJournal = {
      date: Storage.get('notiDate') ? Storage.get('notiDate') : Storage.get('selectedDate'),
      userid: Storage.get('notiUserId') ? Storage.get('notiUserId') : Storage.get('UID')
    };
    SpeedDial.get(getJournal).then(function(res){
      var rocks = [];
      var check = {};
      if(res.code == 0){
        for(var r in res.text.rockIds){
          rocks.push(res.text.rockIds[r]);
          if(res.text.rockIds[r].Rock_Status == 'complete'){
            check[res.text.rockIds[r].Rock_ID] = true;
          }else{
            check[res.text.rockIds[r].Rock_ID] = false;
          }
        }
      }
      $scope.rocks.check = check;
      $scope.items = rocks;
      $scope.loading = false;
    });

    $scope.rocks.date = Storage.get('notiDate') ? Storage.get('notiDate') : Storage.get('selectedDate');
    $scope.rocks.userid = Storage.get('notiUserId') ? Storage.get('notiUserId') : Storage.get('UID');
  });

  $scope.RocksProcess = function(status){
    SpeedDial.saveStatus(status).then(function(res){
      $cordovaToast.showLongBottom(res.text);
      $state.go('app.speeddial');
    });
  };
})

.controller('SpeedDialCtrl', function($scope, $sce, $state, Storage, HowToUse, $ionicModal){
  $scope.$on('$ionicView.enter', function(){
    HowToUse.GetSpeedDialContent().then(function(res){
      if(res.code == 0){
        $scope.SpeedDialContent = $sce.trustAsHtml(res.text.Page_Content);
      }
    });
  });

  $scope.composeJournal = function(){
    Storage.destroy('notiDate');
    Storage.destroy('notiUserId');
    Storage.set('selectedDate', $scope.myDate);
    $state.go('app.questions');
  };

  $ionicModal.fromTemplateUrl('templates/h2u-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal){
    $scope.modal = modal;
  });
  $scope.openModal = function(){
    $scope.modal.show();
  };
  $scope.closeModal = function(){
    $scope.modal.hide();
  };
})

.controller('QuestionsCtrl', function($scope, $rootScope, $sce, Auth, SpeedDial, HowToUse, Storage, $ionicPopup, $ionicModal, $ionicActionSheet, $cordovaPrinter){
  $scope.$on('$ionicView.enter', function(){
    $scope.enableEmailPrint = false;

    var getJournal = {
      date: Storage.get('selectedDate'),
      userid: Storage.get('UID')
    };

    $scope.journal = {};
    $scope.journal.date = Storage.get('selectedDate');
    $scope.journal.userid = Storage.get('UID');

    SpeedDial.get(getJournal).then(function(res){
      if(res.code == 0){
        $scope.enableEmailPrint = true;
        var rocks = {};
        for(var r in res.text.rockIds){
          rocks[r] = res.text.rockIds[r].Rock_ID;
        }
        for(var r in res.text.rockAns){
          rocks[r] = res.text.rockAns[r].Answer;
        }
        $scope.journal.question1 = res.text.journal.Answer_1;
        $scope.journal.question2 = res.text.journal.Answer_2;
        $scope.journal.question3 = res.text.journal.Answer_3;
        $scope.journal.question4 = res.text.journal.Answer_4;
        $scope.journal.question5 = res.text.journal.Answer_5;
        $scope.journal.question6 = res.text.journal.Answer_6;
        $scope.journal.rocks = rocks;
        $scope.journal.date = Storage.get('selectedDate');
        $scope.journal.userid = Storage.get('UID');
        $scope.journal.journalId = res.text.journal.Journal_ID;
      }else{
        $scope.journal = {};
        $scope.journal.date = Storage.get('selectedDate');
        $scope.journal.userid = Storage.get('UID');
      }
    });

    HowToUse.GetSpeedDialContent().then(function(res){
      if(res.code == 0){
        $scope.SpeedDialContent = $sce.trustAsHtml(res.text.Page_Content);
      }
    });
  });

  $ionicModal.fromTemplateUrl('templates/h2u-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal){
    $scope.modal = modal;
  });
  $scope.openModal = function(){
    $scope.modal.show();
  };
  $scope.closeModal = function(){
    $scope.modal.hide();
  };

  function GenerateHTML(res){
    var doc = '';
    doc += '<html>';
    doc += '<body>';
    doc += '<ion-view id="journal">';
    doc += '<ion-content>';
    doc += '<div class="row responsive-md">';
    doc += '<div class="col">';
    doc += '<div class="list">';
    if(res.text.journal.Answer_1){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">Today, I appreciate:</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_1+'</p>';
      doc += '</label>';
    }
    if(res.text.journal.Answer_2){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">Today my dominant feeling will be:</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_2+'</p>';
      doc += '</label>';
    }
    doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
    doc += '<span class="input-label">My 6 Inspired Actions for today are</span>';
    doc += '</label>';

    doc += '<div style="margin:0 0 10px 0;">';
    var i = 1;
    for(var r in res.text.rockAns){
      if(res.text.rockAns[r].Answer){
        doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0;">';
        doc += '<p>'+i+'. '+res.text.rockAns[r].Answer+'</p>';
        doc += '</label>';
      }
      i++;
    }
    doc += '</div>';

    if(res.text.journal.Answer_3){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">Everything else I desire is being delegated to the Universe. Thank you Universe, for handling the following for me!</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_3+'</p>';
      doc += '</label>';
    }
    if(res.text.journal.Answer_4){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">My Wild Hair Intention. Wouldn\'t it be awesome today if:</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_4+'</p>';
      doc += '</label>';
    }
    if(res.text.journal.Answer_5){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">One of my desires has manifested in the most perfect way. It unfolded like this...</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_5+'</p>';
      doc += '</label>';
    }
    if(res.text.journal.Answer_6){
      doc += '<label class="item item-input item-stacked-label" style="background:#e0592b;color:#fff;padding:10px 16px;display:block;">';
      doc += '<span class="input-label">The most important thing for me to focus on today is...</span>';
      doc += '</label>';
      doc += '<label class="item item-input" style="padding:10px 16px;display:block;border:1px solid #ddd;margin:-1px 0 10px 0;">';
      doc += '<p>'+res.text.journal.Answer_6+'</p>';
      doc += '</label>';
    }
    doc += '</div>';
    doc += '</div>';
    doc += '</div>';
    doc += '</ion-content>';
    doc += '</ion-view>';
    doc += '</body>';
    doc += '</html>';
    return doc;
  }

  $scope.OpenPrintClient = function(getJournal){
    if($cordovaPrinter.isAvailable()){
      SpeedDial.get(getJournal).then(function(res){
        if(res.code == 0){
          var html = GenerateHTML(res);
          $cordovaPrinter.print(html);
        }
      });
    }else{
      $ionicPopup.alert({
        title: 'INFO',
        template: 'Printing is not available.'
      });
    }
  };

  $scope.OpenEmailClient = function(getJournal){
    SpeedDial.get(getJournal).then(function(res){
      if(res.code == 0){
        var html = GenerateHTML(res);
        cordova.plugins.email.open({
          subject: 'Inspired Actions - Speed Dial',
          body: html
        });
      }
    });
  };

  $scope.OpenEmailPrint = function(){
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: '<i class="icon ion-printer"></i> Print' },
        { text: '<i class="icon ion-email"></i> Email' }
      ],
      titleText: 'Print/Email',
      cancelText: 'Cancel',
      cancel: function(index){
        return true;
      },
      buttonClicked: function(index){
        var getJournal = {
          date   : Storage.get('selectedDate'),
          userid : Storage.get('UID')
        };
        if(index === 0){
          $scope.OpenPrintClient(getJournal);
        }else if(index === 1){
          $scope.OpenEmailClient(getJournal);
        }
        return true;
      }
    });
  };

  $scope.JournalProcess = function(journal){
    SpeedDial.save(journal).then(function(res){
      if(res.code == 0){
        $scope.enableEmailPrint = true;

        $ionicPopup.alert({
          title: 'SUCCESS',
          template: res.text
        });

        Auth.CurrentUser().then(function(res){
          $rootScope.SetLocalNotify(res.text);
        });

        var getJournal = {
          date: Storage.get('selectedDate'),
          userid: Storage.get('UID')
        };
        SpeedDial.get(getJournal).then(function(res){
          if(res.code == 0){
            $scope.enableEmailPrint = true;
            var rocks = {};
            for(var r in res.text.rockIds){
              rocks[r] = res.text.rockIds[r].Rock_ID;
            }
            for(var r in res.text.rockAns){
              rocks[r] = res.text.rockAns[r].Answer;
            }

            $scope.journal.question1 = res.text.journal.Answer_1;
            $scope.journal.question2 = res.text.journal.Answer_2;
            $scope.journal.question3 = res.text.journal.Answer_3;
            $scope.journal.question4 = res.text.journal.Answer_4;
            $scope.journal.question5 = res.text.journal.Answer_5;
            $scope.journal.question6 = res.text.journal.Answer_6;
            $scope.journal.rocks     = rocks;
            $scope.journal.date      = Storage.get('selectedDate');
            $scope.journal.userid    = Storage.get('UID');
            $scope.journal.journalId = res.text.journal.Journal_ID;
          }
        });
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    }, function(error){
      $ionicPopup.alert({
        title: 'ERROR',
        template: error
      });
    });
  };
})

.controller('MediaCtrl', function($scope, $rootScope, $ionicPlatform, $timeout, $sce, Media){
  $scope.config = {};

  $scope.onVideoPlayerReady = function(API, index){
    $rootScope.videoPlayers[index] = API;
  };

  $scope.onAudioPlayerReady = function(API, index){
    $rootScope.audioPlayers[index] = API;
  };

  $scope.onVideoUpdateState = function(state, index){
    if(state === 'play'){
      for(var i = 0; i < $rootScope.videoPlayers.length; i++){
        if(i !== index){
          $rootScope.videoPlayers[i].pause();
        }
      }
    }
  };

  $scope.onAudioUpdateState = function(state, index){
    if(state === 'play'){
      for(var i = 0; i < $rootScope.audioPlayers.length; i++){
        if(i !== index){
          $rootScope.audioPlayers[i].pause();
        }
      }
    }
  };

  $scope.videoRefresh = function(){
    Media.videos().then(function(res){
      if(res.code == 0){
        var videos = [];
        for(var i = 0; i < res.text.length; i++){
          videos.push({
            title: res.text[i].Video_Title,
            preload: "none",
            sources: [{
              src: $rootScope.Web_EndPoint+"uploads/videos/"+res.text[i].Video_File,
              type: "video/mp4"
            }],
            theme: {
              url: "https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css"
            }
          });
        }
        $scope.config.video = videos;
        $scope.$broadcast('scroll.refreshComplete');
      }
    });
  };

  $scope.audioRefresh = function(){
    Media.audios().then(function(res){
      if(res.code == 0){
        var audios = [];
        for(var i = 0; i < res.text.length; i++){
          audios.push({
            title: res.text[i].Audio_Title,
            preload: "none",
            sources: [{
              src: $sce.trustAsResourceUrl($rootScope.Web_EndPoint+"uploads/audios/"+res.text[i].Audio_File),
              type: "audio/mpeg"
            }],
            theme: {
              url: "https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css"
            }
          });
        }
        $scope.config.audio = audios;
        $scope.$broadcast('scroll.refreshComplete');
      }
    });
  };

  $scope.$on('$ionicView.loaded', function(){
    $scope.videoRefresh();
    $scope.audioRefresh();
  });
})

.controller('StatisticsCtrl', function($scope, $rootScope, Storage, SpeedDial, $ionicSideMenuDelegate, $cordovaToast){

  $scope.statistics = function(date){
    $scope.chartOptions = {
      scales: {
        yAxes: [{
          ticks: {
            min: 0,
            max: 6
          }
        }]
      }
    };
    $scope.labels = ['Pending', 'Complete'];
    $scope.series = ['Pending', 'Complete'];
    $scope.data = [];
    $scope.total = 0;
    $scope.complt = 0;
    $scope.percent = 0;

    $scope.date = $rootScope.FormatDate(date);
    var stats = {
      date: $scope.date,
      userid: Storage.get('UID')
    };

    SpeedDial.GetStats(stats).then(function(res){
      if(res.code == 0){
        $scope.total   = res.TotalRocks;
        $scope.complt  = res.CompleteRocks;
        $scope.percent = ((res.CompleteRocks * 100) / res.TotalRocks).toFixed(0);

        $scope.data = [
          [res.PendingRocks],
          [res.CompleteRocks]
        ];
      }
    });
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.$on('$ionicView.enter', function(){
    $ionicSideMenuDelegate.canDragContent(false);

    var toDay = new Date();
    $scope.statistics(toDay);
  });

  $scope.statsRefresh = function(){
    $scope.statistics($scope.date);
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.onSwipeLeft = function(){
    var todate = new Date()
    var NextDate = new Date($scope.date);
    NextDate.setDate(NextDate.getDate()+1);
    if(NextDate >= todate){
      $cordovaToast.showLongBottom('Can\'t access future date');
      return false;
    }
    $scope.statistics(NextDate);
  };

  $scope.onSwipeRight = function(){
    var todate = new Date();
    var PrevDate = new Date($scope.date);
    PrevDate.setDate(PrevDate.getDate() - 1);
    $scope.statistics(PrevDate);
  };

  $scope.$on('$ionicView.leave', function(){
    $ionicSideMenuDelegate.canDragContent(true);
  });
})

.controller('ResourcesCtrl', function($scope, Resources){
  $scope.buyBtn = 'Buy Now!';
  $scope.freeBtn = 'Download!';
  $scope.buttonColor = '#e0592b';
  Resources.GetButtonContent().then(function(res) {
    if(res.code == 0){
      $scope.buyBtn = res.text.buyBtn.Page_Content;
      $scope.freeBtn = res.text.freeBtn.Page_Content;
      $scope.buttonColor = res.text.buttonColor.Page_Content;
    }
  });
  $scope.$on('$ionicView.enter', function(){
    $scope.events = [];
    $scope.loading = true;
    Resources.events().then(function(res){
      if(res.code == 0){
        $scope.events = res.text;
        $scope.loading = false;
      }
    });
  });

  $scope.eventRefresh = function(){
    $scope.loading = true;
    Resources.events().then(function(res){
      if(res.code == 0){
        $scope.events = res.text;
        $scope.loading = false;
      }
    });
    $scope.$broadcast('scroll.refreshComplete');
  };
})

.controller('ConsultationCtrl', function($scope, Auth, Storage, Consultation, $cordovaToast, $ionicPopup){
  $scope.$on('$ionicView.enter', function(){
    Auth.CurrentUser().then(function(res){
      if(res.code == 0){
        var UserData = res.text;
        Consultation.GetConsultationSubject().then(function(response){
          $scope.consult = {
            subject: response.text.Page_Content,
            query: '',
            state: UserData.User_State,
            mobile: UserData.User_Mobile,
            skype: UserData.User_Skype,
            userid: UserData.User_ID
          };
        });
      }
    });
  });

  $scope.ConsultProcess = function(consult){
    Consultation.save(consult).then(function(res){
      if(res.code == 0){
        $scope.consult.query = '';
        $cordovaToast.showLongBottom(res.text);
        window.open('https://masterpeacecoachingapp.youcanbook.me', '_blank', 'location=no');
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    }, function(error){
      $ionicPopup.alert({
        title: 'ERROR',
        template: error
      });
    });
  };
})

.controller('SupportCtrl', function($scope, Storage, Support, $ionicPopup){
  $scope.$on('$ionicView.enter', function(){
    $scope.support = {
      concern: '',
      userid: Storage.get('UID')
    };
  });

  $scope.SupportProcess = function(support){
    Support.save(support).then(function(res){
      if(res.code == 0){
        $scope.support = {
          concern: '',
          userid: Storage.get('UID')
        };
        $ionicPopup.alert({
          title: 'SUCCESS',
          template: res.text
        });
      }else{
        $ionicPopup.alert({
          title: 'ERROR',
          template: res.text
        });
      }
    }, function(error){
      $ionicPopup.alert({
        title: 'ERROR',
        template: error
      });
    });
  };
})

.controller('AboutCtrl', function($scope, $sce, AboutUs){
  $scope.$on('$ionicView.enter', function(){
    $scope.AboutUsContent = 'Loading about content...';
    AboutUs.GetAboutUsContent().then(function(res){
      if(res.code == 0){
        $scope.AboutUsContent = $sce.trustAsHtml(res.text.Page_Content);
      }
    });
  });
});
