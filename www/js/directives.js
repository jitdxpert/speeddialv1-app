angular.module('starter.directives', [])

.directive('activePageHighlight', function($rootScope, $state) {
	return function($scope, $element, $attr) {
		function checkUISref() {
			if ( $state.is($attr['uiSref']) ) {
				$element.addClass('active-page-highlight');
			} else {
				$element.removeClass('active-page-highlight');
			}
		}

		checkUISref();

		$rootScope.$on('$stateChangeSuccess', function() {
			checkUISref();
		});
	};
})

.directive('input', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      'returnClose': '=',
      'onReturn': '&',
      'onFocus': '&',
      'onBlur': '&'
    },
    link: function(scope, element, attr) {
      element.bind('focus', function(e) {
        if (scope.onFocus) {
          $timeout(function() {
            scope.onFocus();
          });
        }
      });
      element.bind('blur', function(e) {
        if (scope.onBlur) {
          $timeout(function() {
            scope.onBlur();
          });
        }
      });
      element.bind('keydown', function(e) {
        if (e.which == 13) {
          if (scope.returnClose) element[0].blur();
          if (scope.onReturn) {
            $timeout(function() {
              scope.onReturn();
            });
          }
        }
      });
    }
  }
})

.directive('expandingTextarea', function () {
  return {
    restrict: 'A',
    controller: function ($scope, $element, $attrs, $timeout) {
      $element.css('min-height', '0');
      $element.css('resize', 'none');
      $element.css('overflow-y', 'hidden');
      setHeight(24);
      $timeout(setHeightToScrollHeight);

      function setHeight(height) {
        $element.css('height', height + 'px');
        $element.css('max-height', height + 'px');
      }

      function setHeightToScrollHeight() {
        setHeight(24);
        var scrollHeight = angular.element($element)[0].scrollHeight;
        if (scrollHeight !== undefined) {
          setHeight(scrollHeight);
        }
      }

      $scope.$watch(function () {
        return angular.element($element)[0].value;
      }, setHeightToScrollHeight);
    }
  };
})

.directive('myLoading', function($http) {
    return {
        restrict: 'A',
        template: '<div class="my__Loading"><ion-spinner></ion-spinner> Loading&#8230;</div>',  
        link: function(scope, elm, attrs) {
            scope.isLoading = function() {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function(v) {
                if ( v ) {
                  scope.loaderShow = true;
                } else {
                  scope.loaderShow = false;
                }
            });
        }
    };
});
