'use strict';

angular.module('starter.services', [])

.factory('Auth', function($http, $q, $rootScope, Storage){
	var Auth = this;

	Auth.register = function(signup, scope){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SignupProcess', signup, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.SocialLogin = function(data, scope){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SocialSigninProcess', data, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.login = function(signin, scope){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SigninProcess', signin, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.forgot = function(forgot, scope){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=ForgotProcess', forgot, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.logout = function(logout){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=LogoutProcess', logout, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.isLogged = function(){
		var checkLocalStorage = Storage.get('UID');
		return checkLocalStorage;
	};

	Auth.createLogId = function(){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=CreateLogID', Storage.get('UID'), {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.CurrentUser = function(){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=GetUserDetails', Storage.get('UID'), {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.updateDevice = function(device){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=UpdateDevice', device, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.setReminder = function(profile){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=UpdateReminder', profile, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.setReminderTime = function(profile){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=UpdateReminderTime', profile, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.changePassword = function(profile){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=ChangePassword', profile, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	Auth.SaveRegistrationId = function(registrationId){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveRegistrationId', registrationId, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err){
			defer.reject(err);
		});
		return defer.promise;
	};

	return Auth;
})

.factory('Storage', function(){
	var Storage = this;

	Storage.set = function(key, value){
		return window.localStorage.setItem(key, value);
	};

	Storage.get = function(key){
		return window.localStorage.getItem(key);
	};

	Storage.destroy = function(key){
		return window.localStorage.removeItem(key);
	};

	return Storage;
})

.factory('HowToUse', function($http, $q, $rootScope){
	var HowToUse = this;

	HowToUse.GetSpeedDialContent = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetSpeedDialContent', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return HowToUse;
})

.factory('Media', function($http, $q, $rootScope){
	var Media = this;

	Media.videos = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetAllVideos', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Media.audios = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetAllAudios', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return Media;
})

.factory('Resources', function($http, $q, $rootScope){
	var Resources = this;

	Resources.events = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetAllEvents', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Resources.GetButtonContent = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetButtonContent', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return Resources;
})

.factory('Consultation', function($http, $q, $rootScope){
	var Consultation = this;

	Consultation.save = function(consult){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveConsultation', consult, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	Consultation.GetConsultationSubject = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetConsultationSubject', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return Consultation;
})

.factory('Support', function($http, $q, $rootScope){
	var Support = this;

	Support.save = function(support){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveSupport', support, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return Support;
})

.factory('SpeedDial', function($http, $q, $rootScope, Storage){
	var SpeedDial = this;

	SpeedDial.save = function(journal){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveSpeedDial', journal, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.email = function(journal){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=EmailSpeedDial', journal, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.getAll = function(){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=GetAllSpeedDial', Storage.get('UID'), {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.get = function(journal){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=GetSpeedDial', journal, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.saveStatus = function(status){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveStatus', status, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.GetStats = function(stats){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=GetStats', stats, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.GetNotificationTime = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetNotificationTime', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.SaveLocalNotify = function(notify){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=SaveLocalNotify', notify, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	SpeedDial.GetLocalNotificationByUser = function(user){
		var defer = $q.defer();

		$http.post($rootScope.PHP_EndPoint + '?case=GetLocalNotificationByUser', user, {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return SpeedDial;
})

.factory('AboutUs', function($http, $q, $rootScope){
	var AboutUs = this;

	AboutUs.GetAboutUsContent = function(){
		var defer = $q.defer();

		$http.get($rootScope.PHP_EndPoint + '?case=GetAboutUsContent', {
    		headers: {
    			"Content-Type": "application/json",
    			'Cache-Control' : 'no-cache'
    		}
		})
		.success(function(res){
			defer.resolve(res);
		})
		.error(function(err, status){
			defer.reject(err);
		});
		return defer.promise;
	};

	return AboutUs;
});
